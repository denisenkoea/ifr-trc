//
//  ViewController.h
//  IFR
//
//  Created by Eugene Denisenko on 9/14/15.
//  Copyright (c) 2015 Eugene Denisenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>


@end

