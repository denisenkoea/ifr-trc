//
//  ViewController.m
//  IFR
//
//  Created by Eugene Denisenko on 9/14/15.
//  Copyright (c) 2015 Eugene Denisenko. All rights reserved.
//

#import "ViewController.h"
#import "OCNetworkManager.h"
#import "Constants.h"

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webViewMain;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property NSTimer *timer;
@property NSString *urlString;
@property NSString *uuidStr;

@end

@implementation ViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    _uuidStr = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loadNecessaryRequest) name:kLoadNecessaryRequestNotif object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginNotif) name:@"loginNotif" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(logoutNotif) name:@"logoutNotif" object:nil];
}

- (void)loadNecessaryRequest {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *notifToken = [defaults valueForKey:kPushNotificationToken];
    NSString *requestUrl = [defaults objectForKey:kServerUrl];
    
    if (_tfAddress.hidden) {
        _urlString = [NSString stringWithFormat:@"%@?platform=apns&token=%@&guid=%@", requestUrl, notifToken, _uuidStr];
    }
    else {
        _urlString = [NSString stringWithFormat:@"%@%@%@", _tfAddress.text, @"?platform=apns&token=", notifToken];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]];
    [_webViewMain loadRequest:request];
}

//- (void)loginNotif {
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *notifToken = [defaults valueForKey:kPushNotificationToken];
//    NSString *requestUrl = [defaults objectForKey:@"ud_server_url"];
//    NSString *loginSubUrl = [requestUrl stringByReplacingOccurrencesOfString:@"assistant/login" withString:@"assistant#new-request-list-template"];
////    NSString *loginRequestStr = [NSString stringWithFormat:@"http://10.128.1.177/assistant#/new-request-list-template"];
//    NSURL *url = [NSURL URLWithString:@"http://www.adform.com/DirectIntegrationsUI/#/campaigns"];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    [_webViewMain loadRequest:request];
//}

- (void)logoutNotif {
    
    NSString *requestUrl = [[NSUserDefaults standardUserDefaults] objectForKey:kServerUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
    [_webViewMain loadRequest:request];
    [_timer invalidate];
    _timer = nil;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    NSString *urlStr = [NSString stringWithFormat:@"%@", request.URL];
    NSString *urlMethod = [NSString stringWithFormat:@"%@", request.HTTPMethod];

    if ([urlStr containsString:@"assistant/login"] && ![urlStr containsString:@"platform="] && [urlMethod isEqualToString:@"GET"]) {
        
        NSString *notifToken = [[NSUserDefaults standardUserDefaults]valueForKey:kPushNotificationToken];
        
        NSURL *newUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?platform=apns&token=%@&guid=%@", urlStr, notifToken, _uuidStr]];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:newUrl];
        [_webViewMain loadRequest:requestObj];
        return NO;
        
    } else if ([urlStr containsString:@"assistant#"]) {
        [_timer invalidate];
        _timer = nil;
        _timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    } else if ([urlStr containsString:@"logout"]) {
        
        [_timer invalidate];
        _timer = nil;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    NSLog(@"%@", error.description);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self loadNecessaryRequest];
    [textField resignFirstResponder];
    return YES;
}

- (void)timerTick {
    
    if (![[OCNetworkManager sharedInstance]isSubscribedForNotificatios]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kIsRegisteringForNotifications];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [[OCNetworkManager sharedInstance]checkLoginOrLogoutRequest:NO];
}

@end
