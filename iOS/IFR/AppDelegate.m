//
//  AppDelegate.m
//  IFR
//
//  Created by Eugene Denisenko on 9/14/15.
//  Copyright (c) 2015 Eugene Denisenko. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "OCNetworkManager.h"
#import "Constants.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"isLaunchedOnce"]) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObject:@"http://10.128.1.177/assistant/login/"
                                                            forKey:kServerUrl];
    [defaults registerDefaults:appDefaults];
    [defaults synchronize];
    
    [[OCNetworkManager sharedInstance]clearTokenRequest];
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [[OCNetworkManager sharedInstance]clearTokenRequest];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken {

    NSString* newToken = [[[NSString stringWithFormat:@"%@", deviceToken]
                           stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:newToken delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
//    [alert show];
    
    [[NSUserDefaults standardUserDefaults]setValue:newToken forKey:kPushNotificationToken];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsRegisteringForNotifications];
    [[NSUserDefaults standardUserDefaults]synchronize];
    if (![[NSUserDefaults standardUserDefaults]boolForKey:kIsRegisteringForNotifications]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:kLoadNecessaryRequestNotif object:nil];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:kPushNotificationToken];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kIsRegisteringForNotifications];
    [[NSUserDefaults standardUserDefaults]synchronize];
    if (![[NSUserDefaults standardUserDefaults]boolForKey:kIsRegisteringForNotifications]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:kLoadNecessaryRequestNotif object:nil];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification: (NSDictionary *)userInfo {

    application.applicationIconBadgeNumber = 0;
    
    if (application.applicationState == UIApplicationStateActive)
    {
//        AudioServicesPlaySystemSound (1004);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
}

@end
