//
//  main.m
//  IFR
//
//  Created by Eugene Denisenko on 9/14/15.
//  Copyright (c) 2015 Eugene Denisenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
