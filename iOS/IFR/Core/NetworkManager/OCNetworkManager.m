//
//  OCNetworkManager.m
//  OcsicoContacts
//
//  Created by Eugene Denisenko on 10/29/15.
//  Copyright © 2015 Eugene Denisenko. All rights reserved.
//

#import "OCNetworkManager.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
#import "Constants.h"
#import "OCNetworkManager.h"

@implementation OCNetworkManager


+ (OCNetworkManager *)sharedInstance {
    
    static dispatch_once_t pred;
    static OCNetworkManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[OCNetworkManager alloc] init];
    });
    return shared;
}

- (bool)checkLoginOrLogoutRequest:(BOOL)isLogin {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *notifToken = [defaults valueForKey:kPushNotificationToken];
    if (!notifToken) {
        notifToken = @"";
    }
    NSString *serverUrl = [defaults objectForKey:kServerUrl];
    NSString *tokenUpdateUrl = [serverUrl stringByReplacingOccurrencesOfString:@"assistant/login/" withString:@""];
    NSString *requestString = [NSString stringWithFormat:@"%@api/WebApi/PostCheckLogin", tokenUpdateUrl];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"UTF-8" forHTTPHeaderField:@"Accept-Charset"];
    manager.requestSerializer = requestSerializer;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:requestString parameters:@{@"token":notifToken, @"guid":[[[UIDevice currentDevice]identifierForVendor]UUIDString]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseStr = [[NSString alloc]initWithData:responseObject encoding:NSASCIIStringEncoding];
        
        if (isLogin) {
            if ([responseStr isEqualToString:@"true"]) {
//                [[NSNotificationCenter defaultCenter]postNotificationName:@"loginNotif" object:nil];
            } else {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"logoutNotif" object:nil];
            }
            
        } else {
            if ([responseStr isEqualToString:@"false"]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"logoutNotif" object:nil];
            }
        }
        
        NSLog(@"");
        
    } failure:^(AFHTTPRequestOperation * operation, NSError *error) {
        NSLog(@"");

    }];
    
    return YES;
}

- (void)clearTokenRequest {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSString *serverUrl = [defaults objectForKey:kServerUrl];
    NSString *tokenUpdateUrl = [serverUrl stringByReplacingOccurrencesOfString:@"assistant/login/" withString:@""];
    NSString *requestString = [NSString stringWithFormat:@"%@api/WebApi/UpdateToken/", tokenUpdateUrl];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"UTF-8" forHTTPHeaderField:@"Accept-Charset"];
    manager.requestSerializer = requestSerializer;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:requestString parameters:@{@"token":@"", @"guid":[[[UIDevice currentDevice]identifierForVendor]UUIDString]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"");
        
    } failure:^(AFHTTPRequestOperation * operation, NSError *error) {
        NSLog(@"");
        
    }];
}

- (BOOL)isSubscribedForNotificatios {
    
    if  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        return [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    
    else{
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        BOOL deviceEnabled = !(types == UIRemoteNotificationTypeNone);
        return deviceEnabled;
    }
}

@end
