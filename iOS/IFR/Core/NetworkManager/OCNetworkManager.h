//
//  OCNetworkManager.h
//  OcsicoContacts
//
//  Created by Eugene Denisenko on 10/29/15.
//  Copyright © 2015 Eugene Denisenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OCNetworkManager : NSObject

+ (OCNetworkManager *)sharedInstance;
- (bool)checkLoginOrLogoutRequest:(BOOL)isLogin;
- (void)clearTokenRequest;
- (BOOL)isSubscribedForNotificatios;

@end
