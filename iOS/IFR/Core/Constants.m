//
//  ApiMethods.m
//  KinoKosmos
//
//  Created by Olga Dalton on 21/01/14.
//  Copyright (c) 2014 Olga Dalton. All rights reserved.
//

#import "Constants.h"

NSString *const kIsLoggedInUD = @"isLoggedIn";
NSString *const kIsAppRunning = @"isAppRunning";
NSString *const kPushNotificationToken = @"pushNotificationToken";
NSString *const kServerUrl = @"ud_server_url";
NSString *const kIsRegisteringForNotifications = @"isRegisteringForNotifications";
NSString *const kLoadNecessaryRequestNotif = @"loadNecessaryRequestNotif";

