//
//  ApiMethods.h
//  KinoKosmos
//
//  Created by Olga Dalton on 21/01/14.
//  Copyright (c) 2014 Olga Dalton. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kIsLoggedInUD;
extern NSString *const kIsAppRunning;
extern NSString *const kPushNotificationToken;
extern NSString *const kServerUrl;
extern NSString *const kIsRegisteringForNotifications;
extern NSString *const kLoadNecessaryRequestNotif;